package akka.remote.netty;

import akka.actor.ActorPath;
import akka.actor.ExtendedActorSystem;
import akka.remote.RemoteActorRefProvider;
import akka.remote.RemoteMessage;
import akka.remote.RemoteProtocol;
import org.apache.commons.lang3.exception.ExceptionUtils;

import java.lang.reflect.Field;
import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;

import static akka.remote.RemoteProtocol.MessageProtocol;
import static org.nohope.AkkaClassLoadingUtil.defineClassFrom;

/**
 * @author <a href="mailto:ketoth.xupack@gmail.com">ketoth xupack</a>
 * @since 10/7/12 1:29 AM
 */
@SuppressWarnings("unchecked")
public class ClassLoadingNettyTransport extends NettyRemoteTransport {
    public ClassLoadingNettyTransport(final ExtendedActorSystem system,
                                      final RemoteActorRefProvider provider) {
        super(system, provider);
    }

    private static MessageProtocol getProtocol(final RemoteMessage message) {
        try {
            final Field field = RemoteMessage.class.getDeclaredField("input");
            field.setAccessible(true);
            return ((RemoteProtocol.RemoteMessageProtocol) field.get(message)).getMessage();
        } catch (Exception e) {
            throw new IllegalStateException(e);
        }
    }

    private void trySerialize(final RemoteMessage remoteMessage) throws ClassNotFoundException {
        final MessageProtocol protocol = getProtocol(remoteMessage);
        try {
            final Class clazz = Class.forName("akka.remote.MessageSerializer$");
            final Field field = clazz.getDeclaredField("MODULE$");
            final Method deserialize =
                    clazz.getDeclaredMethod("deserialize",
                            ExtendedActorSystem.class,
                            MessageProtocol.class);

            final Object serializer = field.get(clazz);
            deserialize.invoke(serializer, system(), protocol);
        } catch (final InvocationTargetException e) {
            for (final Throwable th :ExceptionUtils.getThrowableList(e.getTargetException())) {
                if (th instanceof ClassNotFoundException) {
                    throw (ClassNotFoundException) th;
                }
            }

            throw new IllegalArgumentException(e.getTargetException());
        } catch (ClassNotFoundException
                | IllegalAccessException
                | NoSuchFieldException
                | NoSuchMethodException e) {
            throw new IllegalStateException(e);
        }
    }

    @Override
    public void receiveMessage(final RemoteMessage remoteMessage) {
        try {
            trySerialize(remoteMessage);
        } catch (final ClassNotFoundException e) {
            // FIXME: hardcoded, no deadLetters check
            final ActorPath child = remoteMessage.sender().path().child("class-reader");
            final String className = e.getMessage();
            defineClassFrom(system().actorFor(child),
                    className,
                    new Runnable() {
                        @Override
                        public void run() {
                            ClassLoadingNettyTransport.super.receiveMessage(remoteMessage);
                        }
                    },
                    system());
            return;
        }

        super.receiveMessage(remoteMessage);
    }
}
