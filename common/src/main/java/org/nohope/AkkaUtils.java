package org.nohope;

import akka.actor.ActorSystem;
import com.typesafe.config.ConfigFactory;
import org.apache.commons.io.IOUtils;

import java.io.IOException;

/**
 * @author <a href="mailto:ketoth.xupack@gmail.com">ketoth xupack</a>
 * @since 8/16/12 10:15 PM
 */
public final class AkkaUtils {

    private AkkaUtils() {
    }

    public static ActorSystem createLocalSystem(final String systemName) {
        return buildSystem(systemName).build();
    }

    public static ActorSystemBuilder buildSystem(final String systemName) {
        return new ActorSystemBuilder(systemName);
    }

    public static ActorSystem createRemoteSystem(final String systemName,
                                                 final String transport,
                                                 final int port) {
        return new ActorSystemBuilder(systemName)
                .put("actor.provider", "akka.remote.RemoteActorRefProvider")
                .put("remote.transport", transport)
                .put("remote.log-sent-messages", "on")
                .put("remote.log-received-messages", "on")
                .put("remote.log-received-messages", "on")
                .put("remote.netty.hostname", "localhost")
                .put("remote.netty.port", port)
                .build();
    }

    public static class ActorSystemBuilder {
        private final StringBuilder config = new StringBuilder();
        private final String name;

        public ActorSystemBuilder(final String name) {
            this.name = name;
        }

        public ActorSystemBuilder put(final String key, final Object value) {
            config.append(key)
                  .append(" = ")
                  .append(value)
                  .append('\n');
            return this;
        }

        public ActorSystem build() {
            final String conf;
            try {
                conf = IOUtils.toString(ClassLoader.getSystemResource("test/akka.conf").openStream());
            } catch (final IOException e) {
                throw new IllegalStateException(e);
            }

            final String config = String.format(conf, this.config.toString());
            return ActorSystem.create(name, ConfigFactory.parseString(config));
        }
    }
}

